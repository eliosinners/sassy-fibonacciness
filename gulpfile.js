const { src, dest, watch, series, parallel } = require("gulp")
const sass = require("gulp-sass")(require("sass"))

function scssTask() {
  return src("./example/example.scss")
    .pipe(
      sass({
        includePaths: ["node_modules"],
      }),
    )
    .pipe(dest("./static"))
}

const build = series(scssTask)

exports.build = build
exports.default = build
