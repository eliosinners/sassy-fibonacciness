# SassyFibonacciness Credits

## Artwork

- [publicdomainvectors:Spiraling-circles](https://publicdomainvectors.org/en/free-clipart/Spiraling-circles-vector-image/34545.html)
- [wikimedia:The_naturalist](https://commons.wikimedia.org/wiki/File:The_naturalist%27s_miscellany,_or_Coloured_figures_of_natural_objects_%28Plate_30%29_%287780973844%29.jpg)

# These were useful

Huge thanks to Hugo Giraudel [at-import](https://github.com/at-import/SassyLists) for the useful SassyLists functions. I also copied your app structure and learnt a lot about how to package and distribute such a thing.
