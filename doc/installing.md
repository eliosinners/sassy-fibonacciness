# Installing SassyFibonacciness

- [SassyFibonacciness Prerequisites](/eliosin/sassy-fibonacciness/prerequisites.html)

## npm

Install into your SASS projects.

```
npm install @elioway/sassy-fibonacciness
yarn add @elioway/sassy-fibonacciness
```

Import the functions into your master scss file.

```
@import "@elioway/sassy-fibonacciness/dist/SassyFibonacciness";
```
